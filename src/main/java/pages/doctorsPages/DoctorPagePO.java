package pages.doctorsPages;

import org.openqa.selenium.By;
import org.testng.Assert;
import pages.BasePO;
import utils.DriverManager;

public class DoctorPagePO extends BasePO {
    private static String url;

    public DoctorPagePO() {
        super(url);
    }

    private final By SIGN_UP_BUTTON_SEL = By.xpath("//button[@class=\"btn btn-success btn-sign btn-modal\"]");

    //зайти в карточку лікаря знаючи місто, призвіще і ім'я
    public DoctorPagePO open(String city, String name) {
        url = "/doctor/" + city + "/";
        return (DoctorPagePO) new DoctorPagePO().open();
    }

    //перевіряємо видимість копки Записатись
    public Boolean signUpButtonIsVisible() {
        Assert.assertTrue(existsElement(SIGN_UP_BUTTON_SEL), "Кнопки \"Записатись немає в DOM\"");
        return DriverManager.getDriver().findElement(SIGN_UP_BUTTON_SEL).isDisplayed();
    }
}
