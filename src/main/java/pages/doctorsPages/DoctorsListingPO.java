package pages.doctorsPages;

import pages.BasePO;

public class DoctorsListingPO extends BasePO {
    private static final String DOCTORS_URL = "/doctors/kiev/all";


    public DoctorsListingPO() {
        super(DOCTORS_URL);
    }
}
