package pages.headerPage.elements;

import org.openqa.selenium.By;
import utils.DriverManager;

import static pages.BasePO.*;

public class SearchBox {
    private final By SEARCH_BOX_SEL = By.xpath("//input[@data-controll=\"search\"]");
    String DOCTOR_TEXT = dependsOnLanguage("Лікар", "Врач");
    private final By DOCTOR_IN_LIST = By.xpath("//div[text()='"+ DOCTOR_TEXT +"']");

    //Шукаємо по пошуковому запиту
    public SearchBox search(String q) {
        focusedElement(SEARCH_BOX_SEL);
        DriverManager.getDriver().findElement(SEARCH_BOX_SEL).clear();
        DriverManager.getDriver().findElement(SEARCH_BOX_SEL).sendKeys(q);
        sleep(500);
        return new SearchBox();
    }

    //вибираємо першого лікаря з випадаючого списку
    public void selectDoctorInList(){
        clickElement(DOCTOR_IN_LIST);
    }
}
