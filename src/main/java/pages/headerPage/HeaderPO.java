package pages.headerPage;

import org.openqa.selenium.By;
import pages.headerPage.elements.SearchBox;

import static pages.BasePO.clickElement;

public class HeaderPO {
    public SearchBox searchBox;
    public HeaderPO() {
        searchBox = new SearchBox();
    }

    private static final By LANGUAGE_BTN = By.xpath("//a[contains(@style,\"color:#8e8e8e;\")]");

    //переключити мову
    public HeaderPO changeLang() {
        clickElement(LANGUAGE_BTN);
        return this;
    }
}
