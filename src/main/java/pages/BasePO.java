package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import java.util.concurrent.TimeUnit;


import static utils.DriverManager.*;
import static utils.PropertiesLoader.getProp;

public abstract class BasePO {


    private final String REL_URL;
    private static String BASE_URL;
    private final String LANG_URL;


    public BasePO(String url) {
        REL_URL = url;

        BASE_URL = System.getProperty("baseUrl");

        if (BASE_URL == null) {
            BASE_URL = getProp("baseUrl");
        }

        switch (BASE_URL) {
            case "prod":
                BASE_URL = getProp("prod");
                break;
            default:
                BASE_URL = getProp("prod");
                break;
        }

        LANG_URL = dependsOnLanguage("/" + getProp("language").toLowerCase(), "");
    }

    private BasePO open(String url) {
        getDriver().get(url);
        return this;
    }


    public BasePO open() {
        String s = BASE_URL + LANG_URL + REL_URL;
        Reporter.log("Перехід на : " + s, true);
        open(s);
        return this;
    }

    public static String getBaseUrl() {
        return BASE_URL;
    }

    //очікування поки елемент стане клікабельним(WebElement)
    public static WebElement waitUntilElementIsClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 30, 100);
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    //очікування поки елемент стане клікабельним(By)
    public static WebElement waitUntilElementIsClickable(By by) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        try {
            return wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(by)));
        } catch (StaleElementReferenceException e) {
            Reporter.log("Був StaleElementReferenceException!", true);
            return wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(by)));
        }
    }


    //очікування поки елемент стане видимим(WebElement)
    public static WebElement waitUntilElementIsDisplayed(WebElement element) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    //якщо елемент існує, то метод повертає true(By)
    public static boolean existsElement(By by) {
        getDriver().manage().timeouts().implicitlyWait(1,
                TimeUnit.SECONDS);
        try {
            getDriver().findElement(by);
            getDriver().manage().timeouts().implicitlyWait(Long.parseLong(getProp("implicitWait")), TimeUnit.SECONDS);
        } catch (NoSuchElementException e) {
            getDriver().manage().timeouts().implicitlyWait(Long.parseLong(getProp("implicitWait")), TimeUnit.SECONDS);
            return false;
        }
        getDriver().manage().timeouts().implicitlyWait(Long.parseLong(getProp("implicitWait")), TimeUnit.SECONDS);
        return true;
    }

    //метод повертає текст по заданому селектору By
    public static String getText(By by) {
        return waitUntilElementIsDisplayed(getDriver()
                .findElement(by))
                .getText();
    }

    //клік на елементі(By)
    public static void clickElement(By by) {
        try {
            waitUntilElementIsClickable(by).click();
        } catch (WebDriverException e) {
            Reporter.log("В ClickElement був " + e.getClass().getSimpleName(), true);
            //e.printStackTrace();
            try {
                WebElement el = getDriver().findElement(by);
                JavascriptExecutor executor = (JavascriptExecutor) getDriver();
                executor.executeScript("arguments[0].click()", el);
            } catch (StaleElementReferenceException r) {
                Reporter.log("В ClickElement відбувся StaleElementReferenceException!!!", true);
                clickElement(by);
            }
        }
    }

    //клік на елементі(WebElement)
    public static void clickElement(WebElement element) {
        try {
            waitUntilElementIsClickable(element).click();
        } catch (WebDriverException e) {
            Reporter.log("В ClickElement був " + e.getClass().getSimpleName(), true);
            try {
                //e.printStackTrace();
                JavascriptExecutor executor = (JavascriptExecutor) getDriver();
                executor.executeScript("arguments[0].click()", element);
            }
            //отримую локатор з елемента, заново шукаю і клікаю
            catch (StaleElementReferenceException r) {
                clickElement(getLocatorFromWebElement(element));
            }
        }
    }

    //отримати атрибут елемента(By)
    public static String getElementAttributeBy(By by, String attribute) {
        return getDriver().findElement(by).getAttribute(attribute);
    }

    //повертає By локатор з WebElement
    private static By getLocatorFromWebElement(WebElement element) {
        String name;
        String str = element.toString();
        Reporter.log("str = " + str, true);

        if (str.contains("xpath")) {
            name = str.substring(str.lastIndexOf(": /") + 1, str.length() - 1);
            Reporter.log("Exception! Локатор = " + name, true);
            return By.xpath(name);
        } else {
            name = str.substring(str.lastIndexOf(":") + 1, str.length() - 1);
            Reporter.log("Exception! Локатор = " + name, true);
            return By.cssSelector(name);
        }
    }

    //перевести фокус на елемент
    public static void focusedElement(By by) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].focus()", getDriver().findElement(by));
    }

    //метод який присвоює правильні тексти залежно від мови
    public static String dependsOnLanguage(String ukrLang, String rusLang) {
        return getProp("language").equalsIgnoreCase("ua") ? ukrLang : rusLang;
    }

    //почекати вказану кількість мілісекунд
    public static void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

