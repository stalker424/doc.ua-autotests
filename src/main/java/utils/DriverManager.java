package utils;

import io.github.bonigarcia.wdm.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static pages.BasePO.getBaseUrl;
import static utils.PropertiesLoader.getProp;

public class DriverManager {

    private static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    private static String browserName = System.getProperty("browser");
    private static String browserType = System.getProperty("driverType");

    public static Boolean acceptSetDriver = false;


    public static WebDriver getDriver() {
        if (driver.get() == null & acceptSetDriver)
            setDriver();
        return driver.get();
    }


    private static void setDriver() {
        try {
            DriverManager.driver.set(configureDriver(init()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.out.println("Remote URL is not set");
        }
    }

    private static WebDriver init() throws MalformedURLException {
        if (browserName != null) {
            System.out.println("Current browser is: " +
                    browserName.toUpperCase());
        } else {
            browserName = getProp("browser");
            System.out.println("Browser is not stated. Default stated browser is: " +
                    browserName.toUpperCase());
        }

        System.out.println("BaseUrl is: " + getBaseUrl());

        if (browserType == null) {
            browserType = getProp("driverType");
        }

        if (browserName.equalsIgnoreCase("chrome"))
            return getChromeInstance();
        if (browserName.equalsIgnoreCase("firefox"))
            return getFirefoxInstance();
        if (browserName.equalsIgnoreCase("opera"))
            return getOperaInstance();
        if (browserName.equalsIgnoreCase("edge"))
            return getEdgeInstance();
        if (browserName.equalsIgnoreCase("ie"))
            return getIEInstance();
        else
            return getChromeInstance();
    }

    private static WebDriver getChromeInstance() throws MalformedURLException {
        if (!browserType.equalsIgnoreCase("remote")) {
            WebDriverManager.chromedriver().setup();
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--start-maximized");
            chromeOptions.addArguments("--ignore-certificate-errors");
            chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("credentials_enable_service", false); //прибирає запит на збереження паролю
            prefs.put("profile.password_manager_enabled", false);

            chromeOptions.setExperimentalOption("prefs", prefs);

            return new ChromeDriver(chromeOptions);

        } else {
            //тут має бути код для віддаленого запуску автотестів
            return new ChromeDriver();
        }
    }

    private static WebDriver getFirefoxInstance() {
        if (!browserType.equalsIgnoreCase("remote")) {
            WebDriverManager.firefoxdriver().setup();
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            return new FirefoxDriver(firefoxOptions);
        } else {
            //тут має бути код для віддаленого запуску автотестів
            return new ChromeDriver();
        }
    }

    private static WebDriver getOperaInstance() {
        if (!browserType.equalsIgnoreCase("remote")) {
            WebDriverManager.operadriver().setup();
            OperaOptions options = new OperaOptions();
            options.addArguments("--start-maximized");

            return new OperaDriver(options);
        } else {
            //тут має бути код для віддаленого запуску автотестів
            return new ChromeDriver();
        }
    }

    private static WebDriver getEdgeInstance() {
        WebDriverManager.edgedriver().setup();
        return new EdgeDriver();
    }

    private static WebDriver getIEInstance() {
        //потрібно добавляти сертифікат щоб працювало(при цьому може злетіти Chrome)
        WebDriverManager.iedriver().setup();
        InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
        //чистимо кеш IE
        capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
        //запуск проксі тільки для одного екземпляра IE
        capabilities.setCapability(InternetExplorerDriver.IE_USE_PER_PROCESS_PROXY, true);
        internetExplorerOptions.merge(capabilities);


        return new InternetExplorerDriver(internetExplorerOptions);
    }


    private static WebDriver configureDriver(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(Long.parseLong(getProp("implicitWait")), TimeUnit.SECONDS);
        return driver;
    }

    //виконати JS скріпт
    public static void runScript(String js) {
        JavascriptExecutor je = (JavascriptExecutor) getDriver();
        je.executeScript(js);
    }
}