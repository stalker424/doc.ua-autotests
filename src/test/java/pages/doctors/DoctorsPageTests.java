package pages.doctors;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.BaseTest;

public class DoctorsPageTests extends BaseTest {
    @Test
    public void signUpButtonIsVisible() {
        doctorsListing.open();
        header.searchBox.search("терапевт").selectDoctorInList();
        Assert.assertTrue(doctorPage.signUpButtonIsVisible());
    }
}
