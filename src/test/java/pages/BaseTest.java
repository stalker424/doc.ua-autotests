package pages;

import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.doctorsPages.DoctorPagePO;
import pages.doctorsPages.DoctorsListingPO;
import pages.headerPage.HeaderPO;
import utils.DriverManager;
import utils.listeners.ScreenshotAndVideoListener;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static utils.DriverManager.*;
import static utils.DriverManager.getDriver;
import static utils.PropertiesLoader.getProp;

@Listeners({ScreenshotAndVideoListener.class})

public abstract class BaseTest {
    protected DoctorsListingPO doctorsListing = new DoctorsListingPO();
    protected DoctorPagePO doctorPage = new DoctorPagePO();
    protected HeaderPO header = new HeaderPO();

    protected static StopWatch stopWatch = new StopWatch();
    protected static final String ANSI_YELLOW = "\u001B[33m";//жовтий колір
    protected static final String ANSI_RESET = "\u001B[0m";//білий колір

    @BeforeSuite
    public void beforeSuite() {
        stopWatch.start(); //старт секундоміра
    }

    @BeforeMethod(alwaysRun = true)
    public void BeforeMethod(Method method) {
        getDriver().manage().timeouts().pageLoadTimeout(Long.parseLong(getProp("pageLoadTimeout")), TimeUnit.SECONDS);
        //вивожу перекрашені назви тестів з часом їх запуску
        System.out.println(ANSI_YELLOW + new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + " Старт теста: " + method.getName() +
                " з класа " + this.getClass().getName() + " (" + stopWatch.toString() + ")" + ANSI_RESET);
    }

    @AfterMethod
    public void afterMethod(ITestResult result) {
        getDriver().manage().deleteAllCookies();
        DriverManager.runScript("window.localStorage.clear();");
    }

    public SessionId session;

    @BeforeTest
    public void beforeTest() {
        acceptSetDriver = true;
        session = ((RemoteWebDriver) getDriver()).getSessionId();
        if (getProp("driverType").equalsIgnoreCase("remote")) {
            getDriver().manage().window().setSize(new Dimension(1920, 1080));

            Point newPoint = new Point(0, 0);  //пофіксив знаходження вікна
            getDriver().manage().window().setPosition(newPoint);
        }
        if (getProp("browser").equalsIgnoreCase("firefox") & getProp("driverType").equalsIgnoreCase("local")) {
            getDriver().manage().window().maximize();
        }
        System.out.println("ID цієї сесії = " + session);

    }


    @AfterTest
    public void afterTest(ITestContext iTestContext) {
        if (getDriver() != null)
            getDriver().quit();
    }
}

