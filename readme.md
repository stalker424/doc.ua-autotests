#### ТЗ: 

Зайти на сторінку лікарів,  
знайти в пошуку терапевта,  
відкрити сторінку першого лікаря,  
дочекатись завантаження,  
перевірити чи є там кнопка записатись.

#### Підтримувані мови сайту:
Українська і російська.

#### Підтримувані браузери:
Chrome, Opera, Firefox, Internet Explorer і Edge.  
Ще можна доробити запуск тестів в Docker контейнерах. Там буде тільки: Chrome, Opera і Firefox.

#### Запуск: 
На даний момент тести можна запускати через IDE і через термінал.
Команда Maven: mvn test -Dtest=DoctorsPageTests -Dtestng.browser=chrome  
або mvn test -Dsurefire.suiteXmlFiles=testng.xml -Dtestng.value=chrome

#### Налаштування:
Головні налаштування знаходяться в файлі doc.properties
